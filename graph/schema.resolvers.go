package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"os"
	"time"

	"gitlab.com/satriahrh/kentongan/graph/generated"
	"gitlab.com/satriahrh/kentongan/graph/model"
	"gitlab.com/satriahrh/kentongan/utils"
)

func (r *mutationResolver) Chat(ctx context.Context, message string) (*model.Chat, error) {
	chat := model.Chat{
		ID:      fmt.Sprintf("%v%v", os.Getpid(), time.Now().UnixNano()),
		Message: message,
	}
	if err := r.Kafka.ChatProduce(ctx, chat); err != nil {
		return &model.Chat{}, err
	}
	return &chat, nil
}

func (r *queryResolver) Chat(ctx context.Context) ([]*model.Chat, error) {
	var chats []*model.Chat
	var err error
	if chats, err = r.Kafka.ChatRetrievePreviously(ctx); err != nil {
		utils.Log(ctx).Error(err)
	}
	return chats, err
}

func (r *subscriptionResolver) Chat(ctx context.Context) (<-chan *model.Chat, error) {
	var chatChannel chan *model.Chat
	var err error
	if chatChannel, err = r.Kafka.ChatConsume(ctx); err != nil {
		utils.Log(ctx).Error(err)
	}
	utils.Log(ctx).Infof("Yes")
	return chatChannel, err
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Subscription returns generated.SubscriptionResolver implementation.
func (r *Resolver) Subscription() generated.SubscriptionResolver { return &subscriptionResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type subscriptionResolver struct{ *Resolver }
