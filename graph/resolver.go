package graph

import (
	"gitlab.com/satriahrh/kentongan/broker/kafka"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct{
	Kafka kafka.Kafka
}
