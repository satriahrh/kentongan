package kafka

import (
	"context"

	"gitlab.com/satriahrh/kentongan/graph/model"
)

type Kafka interface {
	ChatProduce(ctx context.Context, chat model.Chat) error
	ChatConsume(ctx context.Context) (chan *model.Chat, error)
	ChatRetrievePreviously(ctx context.Context) ([]*model.Chat, error)
	Close(ctx context.Context)
}
