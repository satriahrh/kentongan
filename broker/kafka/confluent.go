package kafka

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"gitlab.com/satriahrh/kentongan/graph/model"
	"gitlab.com/satriahrh/kentongan/utils"
)

type Confluent struct {
	topic          string
	producer       *kafka.Producer
	consumer       *kafka.Consumer
	consumerConfig *kafka.ConfigMap
}

func NewConfluent(ctx context.Context) (*Confluent, error) {
	topic := os.Getenv("KAFKA_TOPIC_CHAT")

	producerConfig := &kafka.ConfigMap{
		"bootstrap.servers":    os.Getenv("KAFKA_BROKERS"),
		"group.id":             os.Getenv("KAFKA_GROUP_ID_BASE"),
		"default.topic.config": kafka.ConfigMap{"auto.offset.reset": "earliest"},
	}

	producer, err := kafka.NewProducer(producerConfig)
	if err != nil {
		utils.Log(ctx).Error(err)
		return nil, err
	}

	consumerConfig := &kafka.ConfigMap{
		"bootstrap.servers":               os.Getenv("KAFKA_BROKERS"),
		"group.id":                        os.Getenv("KAFKA_GROUP_ID_BASE"),
		"go.events.channel.enable":        true,
		"go.application.rebalance.enable": true,
	}
	return &Confluent{
		topic:          topic,
		producer:       producer,
		consumerConfig: consumerConfig,
	}, nil
}

func (k *Confluent) Close(ctx context.Context) {
	k.producer.Close()
	_ = k.consumer.Close()
	utils.Log(ctx).Info("Kafka Broker has been closed")
}

func (k *Confluent) ChatProduce(ctx context.Context, chat model.Chat) (err error) {
	deliveryChan := make(chan kafka.Event)
	bt, _ := json.Marshal(chat)
	err = k.producer.Produce(&kafka.Message{TopicPartition: kafka.TopicPartition{Topic: &k.topic, Partition: kafka.PartitionAny}, Value: bt}, deliveryChan)
	event := <-deliveryChan
	message := event.(*kafka.Message)
	if message.TopicPartition.Error != nil {
		utils.Log(ctx).Infof("Delivery failed: %v\n", message.TopicPartition.Error)
	} else {
		utils.Log(ctx).Infof("Delivered message to topic %s [%d] at offset %v\n",
			*message.TopicPartition.Topic, message.TopicPartition.Partition, message.TopicPartition.Offset)
	}
	return
}

func (k *Confluent) ChatRetrievePreviously(ctx context.Context) (chats []*model.Chat, err error) {
	var consumer *kafka.Consumer
	consumerConfig := *k.consumerConfig
	_ = consumerConfig.SetKey("enable.partition.eof", true)
	_ = consumerConfig.SetKey("auto.offset.reset", "earliest")
	_ = consumerConfig.SetKey("group.id", fmt.Sprintf("%-%v", os.Getenv("KAFKA_GROUP_ID_BASE"), time.Now().Unix()))
	if consumer, err = kafka.NewConsumer(&consumerConfig); err != nil {
		utils.Log(ctx).Error(err)
	} else if err = consumer.Subscribe(k.topic, nil); err != nil {
		utils.Log(ctx).Error(err)
	} else {
		defer func() {
			_ = consumer.Close()
			utils.Log(ctx).Info("The new consumer has been closed")
		}()

		run := true
		for run == true {
			select {
			case eventChannel := <-consumer.Events():
				switch event := eventChannel.(type) {
				case kafka.AssignedPartitions:
					consumer.Assign(event.Partitions)
					utils.Log(ctx).Infof("assign")
				case kafka.RevokedPartitions:
					consumer.Unassign()
					utils.Log(ctx).Infof("unassign")
				case *kafka.Message:
					var chat model.Chat
					if chat, err = k.chatUnmarshal(event); err != nil {
						utils.Log(ctx).Error(err)
					} else {
						chats = append(chats, &chat)
					}
					utils.Log(ctx).Infof("%% Message on %s: %s\n", event.TopicPartition, string(event.Value))
					consumer.Commit()
				case kafka.PartitionEOF:
					utils.Log(ctx).Info(event)
					run = false
				case kafka.Error:
					err = event
					utils.Log(ctx).Error(event)
					run = false
				}
			}
		}
	}

	return
}

func (k *Confluent) ChatConsume(ctx context.Context) (chatChannel chan *model.Chat, err error) {
	var consumer *kafka.Consumer
	consumerConfig := *k.consumerConfig
	_ = consumerConfig.SetKey("group.id", fmt.Sprintf("%-%v", os.Getenv("KAFKA_GROUP_ID_BASE"), time.Now().Unix()))
	if consumer, err = kafka.NewConsumer(&consumerConfig); err != nil {
		utils.Log(ctx).Error(err)
	} else if err = consumer.Subscribe(k.topic, nil); err != nil {
		utils.Log(ctx).Error(err)
	} else {
		cc := make(chan *model.Chat)
		chatChannel = cc
		go func() {
			utils.Log(ctx).Info("Began consuming")
			run := true
			for run == true {
				select {
				case eventChannel := <-consumer.Events():
					switch event := eventChannel.(type) {
					case kafka.AssignedPartitions:
						consumer.Assign(event.Partitions)
					case kafka.RevokedPartitions:
						consumer.Unassign()
					case *kafka.Message:
						var chat model.Chat
						if chat, err = k.chatUnmarshal(event); err != nil {
							utils.Log(ctx).Error(err)
						} else {
							cc <- &chat
						}
						utils.Log(ctx).Infof("%% Message on %s: %s\n", event.TopicPartition, string(event.Value))
						consumer.Commit()
					case kafka.Error:
						err = event
						utils.Log(ctx).Error(event)
						run = false
					}
				}
			}
		}()
	}
	return
}

func (k *Confluent) chatUnmarshal(message *kafka.Message) (chat model.Chat, err error) {
	err = json.Unmarshal(message.Value, &chat)
	return
}
