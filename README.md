# Kentongan

## Development

### Minimum Requirements

- [Docker 1.10.0](https://docs.docker.com/engine/install/) 
- [Go 1.12](https://golang.org/doc/go1.12)

### Setup

- Copy `env.sample` to `.env`, you probably do need to configure anything since it comes with default values.
    ```shell script
    cp env.sample .env
    ```

- Set up the docker container for this project. Do the following command assuming you docker engine is running.
Once the output is set, you can interrupt it by Ctrl+C. 
     ```shell script
     docker-compose up
     ```
    You should then do this following command everytime you want to run the application.
    ```shell script
    docker-compose start
    ```
    Do not forget to stop the container or stop the docker engine once you finish testing out the application by `docker-compose stop`.

- You should now be able to run the application by doing this following command.
    ```shell script
    go run app/graphql/main.go
    ```
  
### API Doc

After running the application, you are now be able to access the Graphql Playground from http://localhost:8080/
(depending on where you set the host and port) with schema documentation attached on it.

## Architecture

### Chat Produce
 ![Alt text here](docs/architecture_chat_produce.svg)

### Chat Consume
 ![Alt text here](docs/architecture_consume.svg)

### Chat Retrieve From Beginning
 ![Alt text here](docs/architecture_retrieve.svg)
