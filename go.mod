module gitlab.com/satriahrh/kentongan

go 1.12

require (
	github.com/99designs/gqlgen v0.12.2
	github.com/confluentinc/confluent-kafka-go v1.4.2
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.6.0
	github.com/vektah/gqlparser/v2 v2.0.1
)
