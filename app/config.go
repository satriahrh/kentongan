package app

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"gitlab.com/satriahrh/kentongan/broker/kafka"
	"gitlab.com/satriahrh/kentongan/utils"
)

func Config(callback func(context.Context, kafka.Kafka))  {
	ctx := context.TODO()
	ctx = context.WithValue(ctx, "Request-ID", fmt.Sprintf("application-%v", os.Getpid()))
	logrus.SetFormatter(&logrus.JSONFormatter{
		DisableTimestamp:  false,
		DisableHTMLEscape: false,
		PrettyPrint:true,
	})
	logrus.SetReportCaller(true)
	if err := godotenv.Load(); err != nil {
		utils.Log(ctx).Warning(err)
	}

	signalChannel := make(chan os.Signal)
	signal.Notify(signalChannel, os.Interrupt, os.Kill, syscall.SIGTERM, syscall.SIGINT)

	kfk, err := kafka.NewConfluent(ctx)
	if err != nil {
		utils.Log(ctx).Fatal(err)
	}

	go func() {
		callback(ctx, kfk)
	}()

	<-signalChannel

	kfk.Close(ctx)
}