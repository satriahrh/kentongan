package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/99designs/gqlgen/graphql/playground"
	"gitlab.com/satriahrh/kentongan/app"
	"gitlab.com/satriahrh/kentongan/broker/kafka"
	"gitlab.com/satriahrh/kentongan/graph"
	"gitlab.com/satriahrh/kentongan/graph/generated"
	"gitlab.com/satriahrh/kentongan/utils"
)

const defaultPort = "8080"

func main() {
	app.Config(func(ctx context.Context, kfk kafka.Kafka) {
		graphqlServer := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{kfk}}))
		graphqlServer.AddTransport(&transport.Websocket{})

		http.Handle("/", playground.Handler("GraphQL playground", "/query"))
		http.HandleFunc("/query", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), utils.FieldRequestID, fmt.Sprintf("%v-%v", os.Getpid(), time.Now().Unix()))
			graphqlServer.ServeHTTP(w, r.WithContext(ctx))
		})

		port := os.Getenv("PORT")
		if port == "" {
			port = defaultPort
		}
		utils.Log(ctx).Infof("connect to http://localhost:%s/ for GraphQL playground", port)
		utils.Log(ctx).Info(http.ListenAndServe(":"+port, nil))
	})
}
