package utils

import (
	"context"

	"github.com/sirupsen/logrus"
)

const (
	FieldRequestID = "Request-ID"
)

func Log(ctx context.Context) *logrus.Entry {
	return logrus.WithField("request_id", ctx.Value(FieldRequestID))
}
